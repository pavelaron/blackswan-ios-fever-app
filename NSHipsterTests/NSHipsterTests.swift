//
//  NSHipsterTests.swift
//  NSHipsterTests
//
//  Created by Aron Pavel on 22/03/16.
//  Copyright © 2016 Black Swan. All rights reserved.
//

import XCTest
@testable import NSHipster

class NSHipsterTests: XCTestCase {
    
    var viewController: MainViewController?
    
    override func setUp() {
        super.setUp()
        
        viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MainViewController") as? MainViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMain() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        XCTAssertNotNil(viewController, "Something went very wrong!")
        
        XCTAssertNotNil(viewController!.tableView)
        XCTAssertNotNil(viewController!.refreshControl)
        
        let url = NSURL(string: viewController!.feedUrl)!
        let readyExpectation = expectationWithDescription("ready")
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            self.viewController!.xmlParser = NSXMLParser(contentsOfURL: url)!
            XCTAssertNotNil(self.viewController!.xmlParser)
            
            let canParse = self.viewController!.xmlParser!.parse()
            let message = self.viewController?.refreshControl?.attributedTitle?.string
            
            XCTAssertNotNil(message)
            XCTAssert(canParse || message!.hasPrefix("No connection"), "No warning was shown!")
            
            XCTAssert(canParse, "No connection!")
            
            XCTAssertNotNil(self.viewController!.articles)
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(30, handler: { error in
            
            XCTAssertNil(error, "Error")
            XCTAssert(!(self.viewController!.tableView.numberOfRowsInSection(0) != 0),
                "No entries found in tableView!")
        })
    }
    
    func testPerformanceMain() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
}
