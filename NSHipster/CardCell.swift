//
//  Card.swift
//  NSHipster
//
//  Created by Aron Pavel on 22/03/16.
//  Copyright © 2016 Black Swan. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let layer = subviews[0].layer
        
        layer.shadowColor = UIColor.darkGrayColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.6
        layer.shadowRadius = 2
    }
}