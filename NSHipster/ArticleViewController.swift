//
//  ArticleViewController.swift
//  NSHipster
//
//  Created by Aron Pavel on 23/03/16.
//  Copyright © 2016 Black Swan. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {
    
    @IBOutlet weak var wvContent: UIWebView!
    
    var htmlContent: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let filePath = NSBundle.mainBundle().pathForResource("style", ofType: "css")
        let style = String(data: NSData(contentsOfFile: filePath!)!, encoding: NSUTF8StringEncoding)
        
        navigationController!.navigationBar.tintColor = .whiteColor()
        wvContent.loadHTMLString(createFromTemplate(htmlContent, style: style!), baseURL: nil)
    }
    
    func createFromTemplate(content: String, style: String) -> String {
        
        return "<!doctype html><html><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/><head><style>\(style)</style></head><body>\(content)</body></html>"
    }
}
