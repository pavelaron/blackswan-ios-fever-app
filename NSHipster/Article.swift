//
//  Article.swift
//  NSHipster
//
//  Created by Aron Pavel on 23/03/16.
//  Copyright © 2016 Black Swan. All rights reserved.
//

import UIKit

class Article {
    
    var title: String?
    var author = "Unknown"
    
    var publishDate: NSDate?
    var htmlContent = ""
}