//
//  ViewController.swift
//  NSHipster
//
//  Created by Aron Pavel on 22/03/16.
//  Copyright © 2016 Black Swan. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController, NSXMLParserDelegate {

    let feedUrl = "http://nshipster.com/feed.xml"
    
    var xmlParser: NSXMLParser?
    var fetchedData: String?
    
    var articles = [Article]()
    var currentElement: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        navigationItem.title = "Articles"
        
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        
        refreshControl!.beginRefreshing()
        refresh()
    }
    
    func showMessage(message: String) {
        refreshControl!.attributedTitle = NSAttributedString(string: message)
    }

    func refresh() {
        
        refreshControl!.attributedTitle = NSAttributedString(string: "Loading...")
        
        let url = NSURL(string: feedUrl)!
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            self.xmlParser = NSXMLParser(contentsOfURL: url)!
            self.xmlParser?.delegate = self
            
            self.articles.removeAll()
            dispatch_async(dispatch_get_main_queue()) {
                
                self.refreshControl!.endRefreshing()
                if !(self.xmlParser?.parse() ?? false) {
                    self.showMessage("No connection. Pull to try again")
                }
                else {
                    self.showMessage("Pull to refresh")
                }
            }
        }
    }
    
    func parseDate(from: String) -> NSDate? {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZZZ"
        
        return dateFormatter.dateFromString(from)
    }
 
    func parserDidEndDocument(parser: NSXMLParser) {
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> ArticleCell {
        
        let cellId: String = "ArticleCell"
        let cell: ArticleCell = tableView.dequeueReusableCellWithIdentifier(cellId) as! ArticleCell
        
        let article = articles[indexPath.row]
        
        cell.labelTitle.text = article.title
        cell.labelAuthor.text = "By: \(article.author)"
        
        if let date = article.publishDate {

            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            
            
            let dateString = dateFormatter.stringFromDate(date)
            cell.labelPublish.text = "Published on: \(dateString)"
        }
        else {
            cell.labelPublish.text = ""
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let senderCell = sender as? ArticleCell {
            
            let destination = segue.destinationViewController as! ArticleViewController
            let html = articles[(tableView.indexPathForCell(senderCell)?.row)!].htmlContent
            
            destination.htmlContent = html
        }
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String: String]) {
        
        currentElement = elementName
        
        if currentElement == "item" {
            articles.append(Article())
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        
        let data = string.stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet())
        if (!data.isEmpty && !articles.isEmpty) {
            
            let currentArticle = articles.last!
            
            if currentElement == "title" {
                currentArticle.title = data
            }
            else if currentElement == "author" {
                currentArticle.author = data
            }
            else if currentElement == "pubDate" {
                currentArticle.publishDate = parseDate(data)
            }
            else if currentElement == "description" {
                currentArticle.htmlContent += data
            }
        }
    }
}

