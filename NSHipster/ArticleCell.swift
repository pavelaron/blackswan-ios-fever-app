//
//  ArticleCell.swift
//  NSHipster
//
//  Created by Aron Pavel on 22/03/16.
//  Copyright © 2016 Black Swan. All rights reserved.
//

import UIKit

class ArticleCell: CardCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var labelPublish: UILabel!
}
